﻿using System;
using System.Collections;
using System.Collections.Generic;
using Valve.VR.InteractionSystem;
using UnityEngine;

public class Shooting : MonoBehaviour {

    public GameObject UI;
    public GameObject crosshair;
    public AudioSource gunshot;
    public GameObject target;

    private void Start()
    {
        UI = GameObject.Find("Canvas3");
        crosshair = GameObject.Find("cross");
        target = GameObject.Find("Target");
    }

    // Update is called once per frame
    void Update () {

        //check gun is assembled
        if (UI.activeSelf)
        {
            //cast ray
            Debug.Log("SHOOTING");
            Shoot();
        }
        
	}

    //raycasting logic
    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(crosshair.transform.position, crosshair.transform.right, out hit))
        {
            Debug.DrawRay(crosshair.transform.position, crosshair.transform.right);
            Debug.Log("Ray hit: " + hit.transform.name);
            if (hit.transform.tag == "Target")
            {
                gunshot.Play();
                soundDelay();
                Destroy(target);
            }
        }
    }

    IEnumerator soundDelay()
    {
        yield return new WaitForSeconds(1);
    }

}
