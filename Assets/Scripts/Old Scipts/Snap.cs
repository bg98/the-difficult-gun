﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snap : MonoBehaviour, Collider_helper.collider_help_reciever
{
    public Transform snapObj;
    public Transform SnapTarget;
    //bool snapped = false;
    private IEnumerator noSnap;
    float lastSnapTime;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Snap Obj: "+snapObj);
	}

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("enter");
        if (other.transform.GetInstanceID() == SnapTarget.GetInstanceID())
        {
            if (lastSnapTime + 0.1f < Time.time)
            {
                snapObj.position = SnapTarget.position;
                snapObj.rotation = SnapTarget.rotation;
                snapObj.parent = SnapTarget;
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {

        Debug.Log("exit");
        if (other.transform.GetInstanceID() == SnapTarget.GetInstanceID())
        {
            lastSnapTime = Time.time;
            snapObj.position = transform.position;
            snapObj.rotation = transform.rotation;
            snapObj.parent = transform;
            //StartCoroutine(noSnap);
        }
    }
}
