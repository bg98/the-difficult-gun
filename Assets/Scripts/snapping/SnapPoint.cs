﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapPoint : MonoBehaviour
{
    [SerializeField]
    Transform hand;
    Transform snapPoint;
    [SerializeField,Range(.2f,10f)]
    float unsnapDist;
    float collisionTime;
    float decollisionTime;
    float collisionGrace = 0.5f;

    // Called when a gameobject enters this trigger
    void OnTriggerEnter(Collider other)
    {
        if (other != transform.parent)
        {
            //print("Trigger Called " + other.name);

            // Only do something if nothing is snapped here already
            if (snapPoint == null)
            {
                //print("snap point = null");

                // Make sure that the GameObject colliding has the Snappable tag, so we don't collide with random things
                if (other.gameObject.tag == "Snappable")
                {
                    //print("other object = snappable");

                    // Tell the script that something is now snapped here
                    snapPoint = other.transform;

                    // Set the object to be a child of this and set its position to the center of its parent (local position)
                    transform.parent.parent = snapPoint;
                    transform.parent.position = snapPoint.position + (transform.parent.position - transform.position);
                    //collisionTime = Time.time;

                }
            }
        }
    }

    // Required because (for some reason) without this code the world position of the snappedObject will not be where it should be
    // This code essentially sets the snappedObject to be centered on this GameObject
    void FixedUpdate()
    {
        if (transform.parent.parent == GameObject.Find("RightHand") || transform.parent.parent == GameObject.Find("LeftHand"))
        {
            hand = transform.parent.parent;
        }
        if (snapPoint != null && hand != null)
        {
            if (Vector3.Distance(hand.position+ (transform.position- transform.parent.position), snapPoint.position)>unsnapDist)
            {
                transform.parent.parent = hand;
                transform.parent.localPosition = Vector3.zero;
                snapPoint = null;
            }
        }
    }

}