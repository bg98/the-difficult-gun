﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Objectives : MonoBehaviour {

    public GameObject barrel1;
    public GameObject barrel2;
    public GameObject scope;
    public GameObject backStock;
    public GameObject triggerStock;
    public GameObject UI1;
    public GameObject UI2;

    bool _barrel = false;
    bool _scope = false;
    bool _stock = false;

    // Use this for initialization
    void Start () {
        barrel1 = GameObject.Find("Barrel_1");
        //barrel2 = GameObject.Find("Barrel_2");
        scope = GameObject.Find("Scope");
        backStock = GameObject.Find("Back_Stock");
        triggerStock = GameObject.Find("Trigger_Stock");

        UI1 = GameObject.Find("Canvas2");
        UI2 = GameObject.Find("Canvas3");

        UI1.SetActive(false);
        UI2.SetActive(false);

        //Debug.Log("Found: " + barrel2);
    }
	
	// Update is called once per frame
	void Update () {
        if (_barrel == true && _scope == true && _stock == true)
        {
            //turn on more UI hints once player has assembled the gun
            UI1.SetActive(true);
            UI2.SetActive(true);
        }

        //Checking if correct pieces are snapped together, and turning off those pieces' interactability in VR
        if (barrel1.transform.parent!= null)
        {
            if (barrel1.transform.parent.parent == triggerStock.transform)
            {
                Debug.Log("SNAPPED");
                //disable VR interactions
                barrel1.GetComponent<Interactable>().enabled = false;
                Destroy(barrel1.GetComponent<Throwable>());
                _barrel = true;
            }
        }
        /*if (barrel2.transform.parent != null)
        {
            if (barrel2.transform.parent.parent == triggerStock)
            {
                Debug.Log("SNAPPED");
                //disable VR interactions
                barrel2.GetComponent<Interactable>().enabled = false;
                _barrel = true;
            }
        }*/
        if (scope.transform.parent != null)
        {
            if (scope.transform.parent.parent == triggerStock.transform)
            {
                Debug.Log("SNAPPED");
                //disable VR interactions
                scope.GetComponent<Interactable>().enabled = false;
                Destroy(scope.GetComponent<Throwable>());
                _scope = true;
            }
        }
        if (backStock.transform.parent != null)
        {
            Debug.Log("PARENT: " + backStock.transform.parent.parent);
            if (backStock.transform.parent.parent == triggerStock.transform)
            {
                Debug.Log("SNAPPED");
                //disable VR interactions
                backStock.GetComponent<Interactable>().enabled = false;
                Destroy(backStock.GetComponent<Throwable>());
                _stock = true;
            }
        }
    }
}
