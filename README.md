# The Difficult Gun

Game Demonstration Video: https://youtu.be/iR_5BhoYwMY 

My final year project for Games Programming at Goldsmiths!
A VR experience that aims to comically capture the struggles of assassins when they have to assemble their gun's on the roof with no instructions. 